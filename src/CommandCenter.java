

import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager  {

    public CommandCenter() {
        super();
    }

    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        int max = 0;
        Spaceship sp = null;
        for (int i = 0; i<ships.size() ; i++) {
            if(ships.get(i).getFirePower()> max){
                max =ships.get(i).getFirePower();
                sp = ships.get(i);
            }
        }
        return sp ;
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {

        for (int i = 0; i < ships.size(); i++) {
            String nam = ships.get(i).getName();
            if(nam.equals(name)){
                return ships.get(i);
            }
        }
        return null;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ArrayList<Spaceship> ret = new ArrayList<>();

        int a = 0;

        for (int i = 0; i < ships.size(); i++) {

            if (ships.get(i).getCargoSpace() >= cargoSize) {
                ret.add(a, ships.get(i));
                a++;
            }
        }

        return ret;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {

        ArrayList<Spaceship> ret = new ArrayList<>();
        int a = 0;

        for (Spaceship ship : ships) {
            if (ship.getFirePower() == 0) {
                ret.add(a, ship);
                a++;
            }
        }

        return ret;
    }
}

