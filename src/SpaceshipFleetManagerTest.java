import java.util.ArrayList;

public class SpaceshipFleetManagerTest {

    SpaceshipFleetManager center;

    ArrayList<Spaceship> testList = new ArrayList<>();


    public SpaceshipFleetManagerTest(SpaceshipFleetManager center) {
        this.center = center;
    }

    public static void main(String[] args) {

        int finalGrade = 0;

        SpaceshipFleetManagerTest spaceshipFleetManagerTest = new SpaceshipFleetManagerTest(new CommandCenter());

        ArrayList<Boolean> boolTest = new ArrayList<>();

        boolTest.add(spaceshipFleetManagerTest.getShipByName_shipExists_returnTargetShip());
        boolTest.add(spaceshipFleetManagerTest.getShipByName_shipNotExists_returnNull());
        boolTest.add(spaceshipFleetManagerTest.getMostPowerfulShip_maxExists_returnMax());
        boolTest.add(spaceshipFleetManagerTest.getMostPowerfulShip_maxExists_returnMaxOne());
        boolTest.add(spaceshipFleetManagerTest.getMostPowerfulShip_maxNotExists_returnNull());
        boolTest.add(spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_shipExists_returnShip());
        boolTest.add(spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_shipNotExists_returnNullList());
        boolTest.add(spaceshipFleetManagerTest.getAllCivilianShips_shipExists_returnShipsList());
        boolTest.add(spaceshipFleetManagerTest.getAllCivilianShips_shipNotExists_returnNullList());


        for (int i = 0; i < boolTest.size(); i++) {
            if(boolTest.get(i)){
                System.out.println("Test "+ (i+1)+" good");
                finalGrade++;
            }else{
                System.out.println("Wrong test "+(i+1));
            }
        }

        System.out.println();
        System.out.println("final grade "+ (finalGrade/2));
    }


    private boolean getShipByName_shipExists_returnTargetShip(){

        testList.add(new Spaceship("One", 10, 10, 10));
        testList.add(new Spaceship("Two", 10, 10, 10));
        testList.add(new Spaceship("Three", 10, 10, 10));
        testList.add(new Spaceship("Four", 10, 10, 10));
        testList.add(new Spaceship("Five", 10, 10, 10));

        String testName = "One";
        Spaceship result = center.getShipByName(testList,testName);

        assert result != null;
        if(result.getName().equals(testName)){
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }

    private boolean getShipByName_shipNotExists_returnNull(){

        testList.add(new Spaceship("One", 10, 10, 10));
        testList.add(new Spaceship("Two", 10, 10, 10));
        testList.add(new Spaceship("Three", 10, 10, 10));
        testList.add(new Spaceship("Four", 10, 10, 10));

        String testName = "Five";
        Spaceship result = center.getShipByName(testList,testName);

        if( result == null){
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }


    private boolean getMostPowerfulShip_maxExists_returnMax(){

        testList.add(new Spaceship("One", 0, 0, 10));
        testList.add(new Spaceship("Two", 10, 10, 10));
        testList.add(new Spaceship("Three", 100, 15, 10));
        testList.add(new Spaceship("Four", 200, 1, 10));
        testList.add(new Spaceship("Five", 300, 10, 10));

        Spaceship test = testList.get(4);
        Spaceship result = center.getMostPowerfulShip(testList);

        if( result != null && result.equals(test)){
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }

    private boolean getMostPowerfulShip_maxExists_returnMaxOne(){

        testList.add(new Spaceship("One", 100, 0, 10));
        testList.add(new Spaceship("Two", 10, 10, 10));
        testList.add(new Spaceship("Three", 100, 15, 10));


        Spaceship test = testList.get(0);
        Spaceship result = center.getMostPowerfulShip(testList);

        if( result != null && result.equals(test)){
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }

    private boolean getMostPowerfulShip_maxNotExists_returnNull(){

        testList.add(new Spaceship("One", 0, 0, 10));
        testList.add(new Spaceship("Two", 0, 10, 10));
        testList.add(new Spaceship("Three", 0, 15, 10));


        Spaceship result = center.getMostPowerfulShip(testList);

        if(result == null){
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }

    public boolean getAllShipsWithEnoughCargoSpace_shipExists_returnShip(){

        testList.add(new Spaceship("One", 0, 0, 10));
        testList.add(new Spaceship("Two", 10, 10, 10));
        testList.add(new Spaceship("Three", 100, 15, 10));
        testList.add(new Spaceship("Four", 200, 1, 10));
        testList.add(new Spaceship("Five", 300, 10, 10));


        ArrayList <Spaceship> testCargo = new ArrayList<>();

        testCargo.add(testList.get(1));
        testCargo.add(testList.get(2));
        testCargo.add(testList.get(4));


        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(testList,10);

        if( result != null && result.equals(testCargo)){
            testList.clear();
            return true;
        }
        testList.clear();
        return false;

    }

    public boolean getAllShipsWithEnoughCargoSpace_shipNotExists_returnNullList(){

        testList.add(new Spaceship("One", 0, 0, 10));
        testList.add(new Spaceship("Two", 10, 0, 10));
        testList.add(new Spaceship("Three", 100, 0, 10));
        testList.add(new Spaceship("Four", 200, 1, 10));
        testList.add(new Spaceship("Five", 300, 9, 10));



        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(testList,10);

        if(result == null){
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }

    public boolean getAllCivilianShips_shipExists_returnShipsList(){

        testList.add(new Spaceship("One", 0, 0, 10));
        testList.add(new Spaceship("Two", 10, 10, 10));
        testList.add(new Spaceship("Three", 100, 15, 10));
        testList.add(new Spaceship("Four", 200, 1, 10));
        testList.add(new Spaceship("Five", 300, 10, 10));


        ArrayList <Spaceship> testFirePower = new ArrayList<>();
        testFirePower.add(testList.get(0));


        ArrayList<Spaceship> result = center.getAllCivilianShips(testList);

        if( result != null && result.equals(testFirePower)){
            testList.clear();
            return true;
        }
        testList.clear();
        return false;

    }

    public boolean getAllCivilianShips_shipNotExists_returnNullList(){

        testList.add(new Spaceship("Two", 10, 10, 10));
        testList.add(new Spaceship("Three", 100, 15, 10));
        testList.add(new Spaceship("Four", 200, 1, 10));
        testList.add(new Spaceship("Five", 300, 10, 10));

        ArrayList<Spaceship> result = center.getAllCivilianShips(testList);

        if(result == null){
            testList.clear();
            return true;
        }
        testList.clear();
        return false;

    }

}

