

public class Spaceship {

    private final String name;

    //огневая мощь
    private int firePower;

    //размер грузового трюма
    private int cargoSpace;

    //прочность
    private int durability;

    public Spaceship(String name, int firePower, int cargoSpace, int durability) {
        this.name = name;
        this.firePower = firePower;
        this.cargoSpace = cargoSpace;
        this.durability = durability;
    }

    public int getFirePower() {
        return firePower;
    }


    public String getName() {
        return name;
    }

    public int getCargoSpace() {
        return cargoSpace;
    }
}
