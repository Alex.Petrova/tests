import java.util.ArrayList;

import org.junit.jupiter.api.*;

public class SpaceshipFleetManagerTestLibr {

    SpaceshipFleetManager center;
    ArrayList<Spaceship> testList = new ArrayList<>();


    public SpaceshipFleetManagerTestLibr(SpaceshipFleetManager center) {
        this.center = center;
    }

    public static void main(String[] args) {

        SpaceshipFleetManagerTestLibr spaceshipFleetManagerTestLibr = new SpaceshipFleetManagerTestLibr(new CommandCenter());

        spaceshipFleetManagerTestLibr.getShipByName_shipExists_returnTargetShip();
        spaceshipFleetManagerTestLibr.getShipByName_shipNotExists_returnNull();


    }


    @BeforeEach
    public void testArrayList(){

        testList.add(new Spaceship("One", 0, 0, 10));
        testList.add(new Spaceship("Two", 10, 10, 10));
        testList.add(new Spaceship("Three", 100, 15, 10));
        testList.add(new Spaceship("Four", 200, 1, 10));
        testList.add(new Spaceship("Five", 300, 10, 10));

    }

    @AfterEach
    public void clearArray(){
        testList.clear();
    }



    @Test
    private void getShipByName_shipExists_returnTargetShip(){

        String testName = "One";
        Spaceship result = center.getShipByName(testList,testName);

        Assertions.assertEquals(result.getName(),testName);
    }

    @Test
    private void getShipByName_shipNotExists_returnNull(){

       testList.remove(testList.get(4));

        String testName = "Five";
        Spaceship result = center.getShipByName(testList,testName);

        Assertions.assertNull(result.getName());

    }

}
